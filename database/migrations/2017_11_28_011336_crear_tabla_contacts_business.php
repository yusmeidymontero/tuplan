<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaContactsBusiness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_business', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_business');
            $table->integer('id_contacts');
            $table->string('twitter');
            $table->string('facebook');
            $table->string('instagram');
            $table->text('address');
            $table->integer('country');
            $table->integer('state');
            $table->integer('city');
            $table->datetime('date_create');
            $table->datetime('date_update');
            $table->integer('user_crate');
            $table->integer('user_update');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_business');
    }
}
