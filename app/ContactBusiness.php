<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactBusiness extends Model
{
    protected $fillable = [
        'name','id_business','id_contact', 'facebook', 'instagram', 'address', 'city'
    ];
}
