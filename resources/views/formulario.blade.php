@extends('layouts.admin')
@section('titulo','Confirmar Reserva')
@section('content')
<div class="container">
<br>
        <div class="jumbotron col-lg-12 my-4">
          <p>
          	<h3><font color="red">Reservación en: {{$business[0]->name}}</font></h3>
            <br>
            {{$business[0]->category}}->{{$business[0]->subcategory}}
          	<br>
            Dirección: {{$business[0]->address}}
            <br>
          </p>
          <p>
            <abbr title="Hours">Horario</abbr>:
              @if(!empty($schedules))
                    @foreach($schedules as $schedule)
                        {{$schedule->week}} - {{$schedule->hours}}
                        <br>
                    @endforeach
                  @endif 

          </p>
          <form class="form-inline" role="form">
            <div class="form-group">
              <label class="sr-only" for="date">Fecha</label>
              <input type="date" class="form-control" id="date"
                     placeholder="Fecha">
            </div>
            <div class="form-group">
              <label class="sr-only" for="time">Hora</label>
              <input type="time" class="form-control" id="time" 
                     placeholder="Hora">
            </div>
            <div class="select">
              <select class="form-control" id="people">
                  <option value="1">1 Persona</option>
                  <option value="2">2 Personas</option>
                  <option value="3">3 Personas</option>
                  <option value="4">4 Personas</option>
                  <option value="5">5 Personas</option>
                  <option value="6">6 Personas</option>
                  <option value="7">7 Personas</option>
              </select>
            </div>
          </form>
        </div>
      <!-- Contact Form -->
      <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
      <div class="row">
        <div class="col-lg-12 mb-4">
          <h3><font color="red">Confirmación</font></h3>
          <form name="sentMessage" id="contactForm" novalidate>
            <div class="control-group form-group">
              <div class="controls">
                <label>Nombre:</label>
                <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Teléfono:</label>
                <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Correo Electrónico:</label>
                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Nota (Opcional):</label>
                <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
              </div>
            </div>
            <div id="success"></div>
            <!-- For success/fail messages -->
            <div align="center">
            <button type="submit" class="btn button-danger btn-lg" id="sendMessageButton">¡LISTO!</button>
            </div>
          </form>
        </div>

      </div>
      <!-- /.row -->

    </div>
@endsection
