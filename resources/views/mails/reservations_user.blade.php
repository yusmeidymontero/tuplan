!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
</head>
<body>
    <p>Hola! Usted ha registrado un reservacion en .</p>
    <p>Estos son los datos registrados en la reservación:</p>
    <ul>
        <li>Nombre: {{ $reservations->user->name }}</li>
        <li>Teléfono: {{ $reservations->user->phone }}</li>
        <li>Día: {{ $reservations->user->day }}</li>
        <li>Hora: {{ $reservations->user->time }}</li>
        <li>Cantidad de Personas: {{ $reservations->user->peoples }}</li>
    </ul>
</body>
</html>