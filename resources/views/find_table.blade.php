@extends('layouts.admin')
@section('titulo','Encontrar Mesa')
@section('content')
<div class="container">    
        <div class="jumbotron my-4">
        	<div class="row">
		        <div class="col-md-6 mb-4">
		            <div class="card">
		              <!--img class="card-img-top" src="http://placehold.it/500x325" alt=""-->
		              @if(!empty($images[0]->perfil))
	                    <img class="card-img-top" src="{{ asset($images[0]->perfil) }}" width="500" height="325" alt="">
	                    @else
	                    	<img class="card-img-top" src="{{ asset('images/default.jpg') }}" width="500" height="325" alt="">
	                  @endif  
		            </div>
		        </div>
		        <div class="col-lg-3 col-md-6 mb-4">
		        	<div class="card-body">
		                <h4 class="card-title">{{$business[0]->name}}</h4>
		                <p class="card-text">
		                	{{$business[0]->category}} - {{$business[0]->subcategory}}
		                </p>	
		              </div> 
		        </div>   	
			</div>
       		<div class="row">
       			<div class="col-sm-4">
		          	<input type="date" name="" class="form-control">
		        </div>
		        <div class="col-sm-2">  	
		          	<input type="time" name="" class="form-control">
		        </div>
		        <div class="col-sm-2">  	
		          	<select class="form-control">
		                <option>1 Persona</option>
		                <option>2 Persona</option>
		            </select>
          		</div>
          		<div class="col-sm-2">
          			<a href="{{ url('reservations/'.$business[0]->id) }}" class="btn btn-danger">Encontrar una Mesa</a>
          		</div>
       		</div>	 	
          
        </div>        
	    <div class="row">
	          <div class="col-lg-12 portfolio-item">
	              <div class="card h-100">
	              	<div class="card-body">
	                  <h4 class="card-title">
	                    <h6>Notas</h6>
	                    <!--div class="g-signin2" data-onsuccess="onSignIn"></div-->
	                  </h4>
	                  <p>Información Importante</p>
	                  <h4 class="card-title">
	                    <h6>Ubicación</h6>
	                    <!--div class="g-signin2" data-onsuccess="onSignIn"></div-->
	                  </h4>
	                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.2527778953!2d-68.01379618550334!3d10.241185592686048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e805dad56fd6dd7%3A0x51720adb600a247e!2sC.+C.+La+Granja!5e0!3m2!1ses!2sve!4v1512354226478" width="1050" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
	                  <h4 class="card-title"> Galeria </h4>
	                    <!--div class="g-signin2" data-onsuccess="onSignIn"></div-->
	                  
	                  <header class="jumbotron my-4">
				        <div id="custom-search-input">
				          <div class="row text-center">
				            <div class="col-lg-3 col-md-6 mb-4">
				              <div class="card">
				                <img class="card-img-top" src="http://placehold.it/500x325" alt="">
				              </div>
				            </div>
				            <div class="col-lg-3 col-md-6 mb-4">
				              <div class="card">
				                <img class="card-img-top" src="http://placehold.it/500x325" alt="">
				              </div>
				            </div>
				            <div class="col-lg-3 col-md-6 mb-4">
				              <div class="card">
				                <img class="card-img-top" src="http://placehold.it/500x325" alt="">
				              </div>
				            </div>
				            <div class="col-lg-3 col-md-6 mb-4">
				              <div class="card">
				                <img class="card-img-top" src="http://placehold.it/500x325" alt="">
				              </div>
				            </div>
				          </div>
				        </div>  
      				</header>
      				<h4 class="card-title">Recomendaciones </h4>
     
      					<div class="row">
          					<div class="col-lg-3 col-md-6 mb-4">
            					<div class="card">
              						<img class="card-img-top" src="http://placehold.it/500x325" alt="">
              							<div class="card-body">
                							<h4 class="card-title">Titulo</h4>
                							<p class="card-text">
                								Descripción
                							</p>	
      									</div>
      									<div class="card-footer" align="center">
							                <a href="" class="btn btn-primary">Ver más!</a>
							             </div>
      							</div>
      						</div>			
                		</div>
	              
	              </div>
	          </div>
	    </div>
	</div>
@endsection
