<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    /*public function channel(){
		return $this->belongsto(channel::class);
	}*/

	public function category(){
		return $this->hasmany(category::class);
	}
}
