@extends('layouts.admin')
@section('titulo','Detalles')
@section('content')
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ url('/') }}">TuPlan</a>
        </li>
        <li class="breadcrumb-item active">Detalle</li>
        <li class="breadcrumb-item active">Establecimiento</li>
      </ol>
      <header class="business-subheader">
          <div class="col-lg-12 col-md-8 col-xs-6">
            <div id="custom-search-input" align="center">
              <br><br>
                  <div class="input-group col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <span class="input-group-addon">
                          <strong><i>¿Cuál es tu plan?</i></strong>
                      </span>
                      <input id="search" type="text" class="form-control input-lg" placeholder="Hamburguesas, Moros, Spa, Cervezas.." list="datalist1" />
                      <span class="input-group-btn">
                          <button class="btn button-danger" type="button" onclick="action_search()">
                          <i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
                      </span>
                      <br>
                  </div>
                  <br>
                   <div class="col-lg-6 collapse navbar-collapse pagination justify-content-center">
                    <form class="form-inline my-2 my-lg-0">
                      <button class="btn button-danger" onclick="action_search_category(1)">Restaurantes</button>
                      <button class="btn button-danger" onclick="action_search_category(3)">Salud y Belleza</button>
                      <button class="btn button-danger" onclick="action_search_category(4)">Bares</button>
                      <button class="btn button-danger">Servicios Profesionales</button>
                    </form>  
                  </div>  
                  <datalist id="datalist1">
                    @foreach($categories as $category)
                      <option data-id_category="{{$category->id}}" value="{{$category->name}}">
                      </option>  
                    @endforeach
                    @foreach($subcategories as $subcategory)
                      @if($subcategory->name != "N/A")
                        <option data-id_subcategory="{{$subcategory->id}}" value="{{$subcategory->name}}"></option>
                      @endif  
                    @endforeach
                  </datalist>
              </div>
          </div>
        </header>
        <br>
        <div class="container-fluid">
          <div class="row">
            @if(!empty($result))
            @foreach($result as $busi)
           <div class="col-md-6">
             <div class="card mb-6">
                <div class="card-body">
                  <h2 class="card-title">{{$busi->name}}</h2>
                  <a href=""><font color="red">{{$busi->category}}</font></a>&rarr;<a href=""><font color="red">{{$busi->place}}</font></a>
                  <input type="hidden" id="id_business" name="id_business" value="{{$busi->id}}">
                   <p class="card-text">
                    Dirección: {{$busi->address}}
                        <br>
                        Facebook: {{$busi->facebook}}
                        <br>
                        Instagram: {{$busi->instagram}}
                        <br>
                        @foreach($phones as $phone)
                             @if($phone->id_business == $busi->id)
                                @if($phone->type == "CELULAR" or $phone->type == "LOCAL")
                                    Teléfono: {{$phone->valor}}
                                @endif
                                @if($phone->type == "EMAIL")
                                    <br>
                                    Email: {{$phone->valor}}
                                @endif    
                             @endif
                        @endforeach
                  </p>
                </div>
                <div class="card-footer text-muted">
                  <a href="{{ url('reservations/'.$busi->id) }}" class="btn button-danger btn-lg btn-block">Reservar</a>
                </div>
              </div>    
           </div>
           <div class="col-md-6">
             <div class="card mb-6">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    @if(!empty($images[0]->route))
                      <img class="d-block w-100" src="{{ asset($images[0]->route) }}" width="350px" height="330px">
                      @else
                      <img class="d-block w-100" src="{{ asset('images/default.jpg') }}" width="350px" height="330px">
                     @endif
                  </div>
                  <div class="carousel-item">
                    @if(!empty($images[1]->route))
                        <img class="d-block w-100" src="{{ asset($images[1]->route) }}" alt="Second slide" width="350px" height="330px">
                        @else
                        <img class="d-block w-100" src="{{ asset('images/default.jpg') }}" width="350px" height="330px">
                    @endif
                  </div>
                  <div class="carousel-item">
                    @if(!empty($images[2]->route))
                      <img class="d-block w-100" src="{{ asset($images[2]->route) }}" alt="Third slide" width="350px" height="330px">
                    @else
                      <img class="d-block w-100" src="{{ asset('images/default.jpg') }}" width="350px" height="330px">
                    @endif  
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
                </div>
              </div>
             </div>
           </div>
           @endforeach
          @endif
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="card mb-6">
                  <div class="card-body">
                  <h5 class="card-title">Descripción</h5>
                  <div class="card-body">
                    @if(!empty($schedules))
                      @foreach($schedules as $schedule)
                          <p>{{$schedule->week}}</p>
                          <p>{{$schedule->hours}}</p>
                      @endforeach
                    @endif 
                  </div>
                  </div>
              </div>
            </div>
            <div class="col-md-6"> 
              <div class="card mb-6">
                  <div class="card-body">
                  <h5 class="card-title">Horario</h5>
                  <div class="card-body">
                    @if(!empty($schedules))
                      @foreach($schedules as $schedule)
                          <p>{{$schedule->week}}</p>
                          <p>{{$schedule->hours}}</p>
                      @endforeach
                    @endif 
                  </div>
                  </div>
              </div>
            </div>  
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="card mb-6">
                <div class="card-body">
                  <h5 class="card-title">Menú</h5>
                </div>
              </div> 
            </div>
          </div>
        <br>   
    <script type="text/javascript">
      function reservarServicio(){
        var id_business = $('#id_business').val();
        $(function(){
            $.ajax({
                type:'get',
                url: '/reservar/'+id_business,
                success: function(data){
                }
            });
        });
      }
    </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
                console.log("Llego");
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
    <script type="text/javascript">
      function action_search(){
           var val = $('#search').val();
           var url = '/search/filter/'+val;
           window.location = url;
      }
      function clean(){
        $('#search').val("");
      }
    </script>

    <script type="text/javascript">
      function action_search_category(category){
        var url = '/search/'+category;
        window.location = url;
      }
    </script>    
@endsection
