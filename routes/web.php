<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/home', 'HomeController@index');
Route::Resource('/business','BusinessController');
Route::get('/','CategoryController@index');
Route::get('/promotions/{day}','CategoryController@promotions');
Route::get('/list_prices','PriceController@ListPrices');
Route::get('/list_provinces','ProvinceController@ListProvinces');
Route::get('/search/{category}','BusinessController@searchBusinessCategory');
Route::get('/search/zone/{input_text}/{zone}','BusinessController@searchBusinessZone');
Route::get('/search/filter/{input_text}','BusinessController@searchBusiness');
Route::get('/search/business/detail/{id}','BusinessController@searchBusinessDetail');
Route::get('business/detail/{id}','BusinessController@business');
Route::get('/reservations/{id}','BusinessController@formularioReservar');
Route::get('/find_table/{id}','BusinessController@findTable');
