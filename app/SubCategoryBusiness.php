<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoryBusiness extends Model
{
    protected $fillable = [
    	'id', 'name'
    ];
}