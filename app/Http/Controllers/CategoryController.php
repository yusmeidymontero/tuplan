<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\SubCategory;
use App\Business;
use App\Contact;
use App\Photo;
use App\Province;
use App\SubCategoryBusiness;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $categories = Category::select('id','name','type')->get();
        $subcategories = SubCategoryBusiness::select('id','name')
                        ->from('business_subcategories')
                        ->get();
        /*POR DEFECTO SE BUSCAN LAS PROMOCIONES DE LOS DIAS LUNES DE TODOS LOS ESTABLECIMIENTOS*/                
        $promotions = Business::select('businesses.id','businesses.name', 'photos.perfil')
            ->join('promotions', 'promotions.id_business', '=', 'businesses.id')
            ->join('photos','photos.id_business','=','promotions.id_business')
            ->where('promotions.days','=','1')
            ->where('photos.perfil','<>','')
            ->distinct()
            ->paginate(9); 

         $business = Business::select('businesses.id', 'businesses.name', 'contact_business.facebook', 'contact_business.instagram','contact_business.address','categories.name as category', 'sub_categories.name as subcategory')
                                ->join('categories','categories.id','=','businesses.id_category')
                                ->join('contact_business','contact_business.id_business','=','businesses.id')
                                ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory')
                                ->get();
            $array = array();   
            $arrayimage = array();

            foreach($business as $value) {
               $phones = Contact::select('id', 'id_business', 'type', 'valor')
                        ->where('id_business', '=', $value->id)
                        ->get();
                array_push($array,$phones); 

                $images = Photo::select('id_business','perfil')
                            ->where('id_business','=',$value->id)
                            ->get(); 
                array_push($arrayimage, $images);                         
            }

        $provinces = Province::select('id','code','name')->where('code','=','GUA')->get();
        $promo_category = 1;
        $promo_day = 1;

        return view('welcome')->with(array('categories' => $categories, 'subcategories'=>$subcategories, 'provinces' => $provinces))->with('all',$business)->with('phones',$array)->with('images',$arrayimage)->with('promotions',$promotions)->with('promo_category',$promo_category)->with('promo_day',$promo_day);
    }

    public function promotions($day){
              
        $promotions = Business::select('businesses.id','businesses.name', 'photos.perfil')
            ->join('promotions', 'promotions.id_business', '=', 'businesses.id')
            ->join('photos','photos.id_business','=','promotions.id_business')
            ->where('promotions.days','=',$day)
            ->where('photos.perfil','<>','')
            ->distinct()
            ->paginate(9); 

        return view('promotions/list_promotions')->with('promotions',$promotions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
