<?php

namespace App\Http\Controllers;

use App\ContactBusiness;
use Illuminate\Http\Request;

class ContactBusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactBusiness  $contactBusiness
     * @return \Illuminate\Http\Response
     */
    public function show(ContactBusiness $contactBusiness)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactBusiness  $contactBusiness
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactBusiness $contactBusiness)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactBusiness  $contactBusiness
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactBusiness $contactBusiness)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactBusiness  $contactBusiness
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactBusiness $contactBusiness)
    {
        //
    }
}
