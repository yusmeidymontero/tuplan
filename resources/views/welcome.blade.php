<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tu Plan</title>
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="css/business-frontpage.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body>
    <!-- Navigation -->
    <!--nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">Tu Plan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav-->
    <!-- Header with Background Image -->
    <header class="business-header">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-8 col-xs-6">
                <div style="position:relative" align="center">
                    <img src="images/logo_tuplan.png" width="248" hspace="16" height="268" vspace="16" />
                    <div style="position:absolute; top:0; left:0;">
                    </div>
                  </div>
                <div id="custom-search-input" align="center">
                  <div class="input-group col-lg-10">
                      <span class="input-group-addon">
                          <strong><i>¿Cuál es tu plan?</i></strong>
                      </span>
                      <input id="search" type="text" class="form-control input-lg" size=40 style="height:50px" placeholder="Hamburguesas, Moros, Spa, Cervezas.." list="datalist1" />
                      <!--div class="input-group-btn search-panel" id="list_prices"></div-->
                      <span class="input-group-btn">
                          <button class="btn button-danger" type="button" onclick="action_search()">
                          <i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
                      </span>
                      <br>
                  </div>
                  <br>
                  
                  <div class="col-lg-6 collapse navbar-collapse pagination justify-content-center">
                    <form class="form-inline my-2 my-lg-0">
                      <button class="btn button-danger" onclick="action_search_category(1)">Restaurantes</button>
                      <button class="btn button-danger" onclick="action_search_category(3)">Salud y Belleza</button>
                      <button class="btn button-danger" onclick="action_search_category(4)">Bares</button>
                      <button class="btn button-danger">Servicios Profesionales</button>
                    </form>  
                  </div>

                  <!--div class="btn-toolbar col-lg-6" align="center" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group btn-group-justified my-btn-group-responsive" role="group" aria-label="First group" align="center">
                          <button type="button" class="btn button-danger fa fa-cutlery col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" onclick="action_search_category(1)"> 
                          Restaurantes</button>
                          <button type="button" class="btn button-danger fa fa-heart col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" onclick="action_search_category(3)"> 
                          Salud y Belleza</button>
                          <button type="button" class="btn button-danger fa fa-glass col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" onclick="action_search_category(4)"> 
                          Bares</button>
                          <button type="button" class="btn button-danger fa fa-tool col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"> 
                          Servicios Profesionales</button>
                        </div>
                  </div-->   
                  <datalist id="datalist1">
                    @foreach($categories as $category)
                      <option data-id_category="{{$category->id}}" value="{{$category->name}}">
                      </option>  
                    @endforeach
                    @foreach($subcategories as $subcategory)
                      @if($subcategory->name != "N/A")
                        <option data-id_subcategory="{{$subcategory->id}}" value="{{$subcategory->name}}"></option>
                      @endif  
                    @endforeach
                  </datalist>
              </div>
            </div>
          </div>
        </header>
    <!-- Page Content -->
    <div class="container">
    <h2 class="my-4" align="center">Promociones del día</h2>
    <nav class="navbar navbar-expand-lg navbar-light bg-faded">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse pagination justify-content-center" id="navbarSupportedContent">
        <form class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(1)">Lunes</button>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(2)">Martes</button>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(3)">Miercoles</button>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(4)">Jueves</button>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(5)">Viernes</button>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(6)">Sábado</button>  
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="enviar_dia(7)">Domingo</button>
            <input type="text" name="dia_activo" id="dia_activo" hidden="" value="1">  
        </form>
      </div>
    </nav>
    <div id="modal" align="center">
      <img src="images/loader.gif" id="loader" width="50" hspace="16" height="50" vspace="16" />
    </div>
    <div id="promotions">
      @if (count($promotions) > 0)
        <section class="block-promotions" id="block-promotions">
          @include('promotions.list_promotions')
        </section>
      @endif

      <!--div class="row" id="block-promotions">
        @foreach($promotions as $promo)
        <div class="col-sm-4 my-4">
          <div class="card">
              @if(is_null($promo->perfil))
              <img class="card-img-top" src="http://placehold.it/300x200" alt="">
              @else
              <img class="card-img-top" src="{{ asset($promo->perfil) }}" width="300" height="200" alt="">
              @endif
            <div class="card-body">
              <h4 class="card-title">{{$promo->name}}</h4>
            </div>
            <div class="card-footer" align="center">
                <a href="{{ url('reservations/'.$promo->id) }}" class="btn button-danger btn-lg btn-block">RESERVAR</a>
            </div>
          </div>
        </div>
        @endforeach
      </div-->
    </div>
    <!--div class="pagination justify-content-center">
        {{ $promotions->links() }}
    </div-->  
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <div class="row">
        <div class="col-xs-6 col-sm-4 text-white"><strong>Acerca de nosotros</strong>
          <br>
          <p class="m-0 text-justific text-white">
            Tu Plan es la guía digital más importante del país, por ser una verdadera herramienta de búsqueda de establecimientos en la ciudad. Permite a sus usuarios tener un máximo conocimiento de los establecimientos, de ofertas, y promociones en distintos bienes y servicios y que además brinda la oportunidad de hacer reservas en línea. Nos motiva ser un aliado de los establecimientos de la ciudad a brindarles visibilidad en canales digitales y generarles ventas asegurándoles un retorno a su inversión. Tu Plan es un producto de la empresa Socialytics.
          </p>
        </div>
        <div class="col-xs-6 col-sm-2 text-white"><strong>Categorías</strong>
          <p text-white>Restaurantes</p>
          <p text-white>Bares</p>
          <p text-white>Salud y Belleza</p>
          <p text-white>Servicios Profesionales</p>
        </div>
        <div class="col-xs-6 col-sm-2 text-center text-white"><strong>Síguenos</strong>  
          <p class="text-center"><a href="#" class="fa fa-facebook fa-2x"></a>
              <a href="#" class="fa fa-instagram fa-2x"></a></p>
        </div>
        <div class="col-xs-6 col-sm-3 text-white"><strong>Descarga nuestra app</strong>
          <p text-white>Próximamente.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-sm-6 text-center text-white"><strong>¿Interesado en ser parte de nuestro grupo de establicimientos?</strong>
          <p text-white>Contáctanos al 0985065657 o a ventas@tuplan.com</p>
        </div>
      </div>
        <p class="m-0 text-center text-white">Copyright &copy; Tuplan-Guayaquil</p>
      </div>
      <!-- /.container -->
    </footer>
        <!-- Bootstrap core JavaScript -->
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <script language="javascript" type="text/javascript">  
        $(document).ready(function() 
            { 
            
              
              $('#search').keypress(function(e){ 
              var text =  $('#search').val();              
               if(e.which == 13){
                  if (!text==null || !text.trim()=="") { 
                    action_search();
                  }          
               }   
              });    
              //$("#search").bind('input', function () {
                //var text =  $('#search').val(); 
                  //if (!text==null || !text.trim()=="") { 
                //    action_search();
                  //} 
              //});
           });  
    </script>
    <script type="text/javascript">
      function action_search(){
           var val = $('#search').val();
           var url = '/search/filter/'+val;
           window.location = url;
      }
      function clean(){
        $('#search').val("");
      }
    </script>

    <script type="text/javascript">
      function action_search_category(category){
        var url = '/search/'+category;
        window.location = url;
      }
    </script>
    <script type="text/javascript">
      function enviar_dia(day){
        openModal();
        $(function(){
            $.ajax({
                type:'get',
                url: '/promotions/'+day,
                success: function(data){
                  document.getElementById("block-promotions").remove();
                    $('#block-promotions').empty().html(data);
                    $('#day_activo').val(day);
                    closeModal();
                }
            });
        });
      }
    </script>
    <script type="text/javascript">

      $(function() {
          $('body').on('click', '.pagination', function(e) {
              e.preventDefault();
              openModal();
              var url = '/';  
              getPromotions(url);
              window.history.pushState("", "", url);
          });

          function getPromotions(url) {
              $.ajax({
                  url : url  
              }).done(function (data) {
                  $('.block-promotions').html(data);
                  closeModal();  
              }).fail(function () {
                  //alert('promotions could not be loaded.');
              });
          }
      });
    </script>
    <script type="text/javascript">
      function openModal() {
        document.getElementById('modal').style.display = 'block';
      }

      function closeModal() {
          document.getElementById('modal').style.display = 'none';
      }
    </script>
  </body>
</html>
