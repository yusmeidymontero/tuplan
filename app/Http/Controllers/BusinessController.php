<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Business;
use App\Contact;
use App\Photo;
use App\Schedule;
use App\Province;
use App\SubCategoryBusiness;
use DB;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function searchBusiness($input_text)
    {
        $words = explode(' ', $input_text);

        $business = Business::select('businesses.id', 'businesses.name','businesses.description', 'contact_business.facebook', 'contact_business.instagram', 'contact_business.address', 'contact_business.place', 'categories.name as category', 'sub_categories.name as subcategory' )
            ->from('categories')
            ->join('businesses','businesses.id_category','=','categories.id')
            ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory') 
            ->join('contact_business','contact_business.id_business','=','businesses.id')
            ->where(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->where('categories.name','like','%'.$word.'%');
                }
            })
            ->orwhere(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhere('businesses.name','like','%'.$word.'%');
                }
            })
            ->orwhere(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhere('businesses.description','like','%'.$word.'%');
                }
            })
            ->orwhere(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhere('sub_categories.name','like','%'.$word.'%');
                }
            })
            ->orwhere(function ($query) use ($words) {
                foreach($words as $word) {
                $query->orwhere('contact_business.place','like','%'.$word.'%');
                }
            })
            ->paginate(5);

            $array = array();                    
            foreach($business as $value) {
               $phones = Contact::select('id', 'id_business', 'type', 'valor')
                        ->where('id_business', '=', $value->id)
                        ->get();
                array_push($array,$phones);             
            }

            $array2 = array();
            foreach ($business as $value) {
                $images = Photo::select('id_business','perfil')
                            ->where('id_business','=',$value->id)
                            ->get(); 
                array_push($array2, $images);
            }

            $categories = Category::select('id','name','type')->get();
            $subcategories = SubCategoryBusiness::select('id','name')
                        ->from('business_subcategories')
                        ->get();

        return view('home')->with('search',$business)->with('phones',$array)->with('images',$array2)->with('categories',$categories)->with('subcategories',$subcategories)->with('search_text',$input_text);
    }

    public function searchBusinessCategory($id_category)
    {

        $business = Business::select('businesses.id', 'businesses.name','businesses.description', 'contact_business.facebook', 'contact_business.instagram', 'contact_business.address', 'contact_business.place', 'categories.name as category', 'sub_categories.name as subcategory' )
            ->from('categories')
            ->join('businesses','businesses.id_category','=','categories.id')
            ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory') 
            ->join('contact_business','contact_business.id_business','=','businesses.id')
            ->where('categories.id','=',$id_category)->paginate(5);

            $array = array();                    
            foreach($business as $value) {
               $phones = Contact::select('id', 'id_business', 'type', 'valor')
                        ->where('id_business', '=', $value->id)
                        ->get();
                array_push($array,$phones);             
            }

            $array2 = array();
            foreach ($business as $value) {
                $images = Photo::select('id_business','perfil')
                            ->where('id_business','=',$value->id)
                            ->get(); 
                array_push($array2, $images);
            }

        $categories = Category::select('id','name','type')->get();
        $subcategories = SubCategoryBusiness::select('id','name')
                        ->from('business_subcategories')
                        ->get();
        $type_category = Category::select('name')->where('id','=',$id_category)->get(); 
        $type;
        foreach($type_category as $value) {
            $type = $value->name;
        }               

        return view('home')->with('search',$business)->with('phones',$array)->with('images',$array2)->with('categories',$categories)->with('subcategories',$subcategories)->with('search_text',$type);
    }

    public function searchBusinessZone($input_text, $zone)
    {
        $words = explode(' ', $input_text);

        $business = Business::select('businesses.id', 'businesses.name','businesses.description', 'contact_business.facebook', 'contact_business.instagram', 'contact_business.address', 'contact_business.place', 'categories.name as category', 'sub_categories.name as subcategory')
            ->from('categories')
            ->join('businesses','businesses.id_category','=','categories.id')
            ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory') 
            ->join('contact_business','contact_business.id_business','=','businesses.id')
            ->where(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhereraw('sub_categories.name like'.'"%'.$word.'%"');
                }
            })->orwhere(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhereraw('0 = '.'"'.$word.'"');
                }
            })
            ->where(function ($query) use ($words) {
                foreach($words as $word) {
                    $query->orwhereraw('categories.name like'.'"%'.$word.'%"');
                }
            })
            ->where('contact_business.place','=', $zone)->paginate(5);

            $html='';
            $html.='<div id="search_result">
                        <div id="search_remove">';
            foreach ($business as $value) {
                $html.='<div class="card mb-4">';
                $html.='<div class="card-body">';
                $html.='<div class="row">';
                $html.='<div class="col-lg-4">';

                $images = Photo::select('id_business','perfil')
                            ->where('id_business','=',$value->id)
                            ->where('perfil','<>','')
                            ->get();           

                if(is_null($images[0]->perfil))
                    $html.='<img class="card-img-top" src="http://placehold.it/250x100" alt="">';
                else
                    $html.='<img class="card-img-top" src="../../'.$images[0]->perfil.'" width="300" height="200" alt="">';

                $html.='</div>';
                $html.='<div class="col-lg-8">';
                $html.='<h2 class="card-title"><a href="business/detail/'.$value->id.'"><font color="red"> '.$value->name.'</font></a></h2>';
                $html.='<p class="card-text">';
                $html.= $value->category.' / '.$value->subcategory;
                $html.='<br>';
                $html.='Dirección: '.$value->address;
                $html.='';
                $html.='<br>';
                $html.='Zona:'. $value->place;
                $html.='</p>';

                $html.='<a href="business/detail/'.$value->id.'" class="btn button-danger">Reservar &rarr;</a>';
                $html.='</div>';
                $html.='</div>';
                $html.='</div>';
                $html.='</div>';
            }
            $html.='<div class="pagination justify-content-center">
            '.$business->links().'</div></div></div>';

        return $html;
    }

    public function business($id){
        $business = Business::select('businesses.id', 'businesses.name', 'contact_business.facebook', 'contact_business.instagram','contact_business.address','contact_business.place','categories.name as category', 'sub_categories.name as subcategory')
                    ->join('categories','categories.id','=','businesses.id_category')
                    ->join('contact_business','contact_business.id_business','=','businesses.id')
                    ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory')
                    ->where('businesses.id','=', $id)
                    ->get();  

        $phones = Contact::where('id_business', '=', $id)
                        ->get();
        
        $schedules = Schedule::where('id_business', '=', $id)->get();

        $images = Photo::where('id_business','=',$id)->get();

        $categories = Category::select('id','name','type')->get();
        $subcategories = SubCategoryBusiness::select('id','name')
                        ->from('business_subcategories')
                        ->get();
                              
        return view('detail_business')->with('result',$business)->with('phones',$phones)->with('schedules',$schedules)->with('images',$images)->with('categories',$categories)->with('subcategories',$subcategories);
    }

    public function searchBusinessDetail($id_business){
        $business = Business::select('businesses.id', 'businesses.name', 'businesses.description', 'contact_business.facebook', 'contact_business.instagram','contact_business.address','categories.name as category', 'sub_categories.name as subcategory')
                    ->join('categories','categories.id','=','businesses.id_category')
                    ->join('contact_business','contact_business.id_business','=','businesses.id')
                    ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory')
                    ->where('businesses.id','=', $id_business)
                    ->get();          
                           
        $phones = Contact::where('id_business', '=', $id_business)
                        ->get();
                               
        $schedules = Schedule::where('id_business', '=', $id_business)->get();

        $images = Photo::where('id_business','=',$id_business)->get();
                              
        return view('detail_business')->with('result',$business)->with('phones',$phones)->with('schedules',$schedules)->with('images',$images);
    }

    public function formularioReservar($id_business){
        $business = Business::select('businesses.id', 'businesses.name', 'contact_business.facebook', 'contact_business.instagram','contact_business.address','categories.name as category', 'sub_categories.name as subcategory')
                    ->join('categories','categories.id','=','businesses.id_category')
                    ->join('contact_business','contact_business.id_business','=','businesses.id')
                    ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory')
                    ->where('businesses.id','=', $id_business)
                    ->get(); 

        $schedules = Schedule::where('id_business', '=', $id_business)->get();                       

        $phones = Contact::where('id_business', '=', $id_business)
                        ->get();            

        return view('formulario')->with('business', $business)->with('phones',$phones)->with('schedules',$schedules);
    }

    public function findTable($id_business){
        $business = Business::select('businesses.id', 'businesses.name', 'contact_business.facebook', 'contact_business.instagram','contact_business.address','prices.name as price','categories.name as category', 'sub_categories.name as subcategory')
                    ->join('categories','categories.id','=','businesses.id_category')
                    ->join('contact_business','contact_business.id_business','=','businesses.id')
                    ->join('sub_categories','sub_categories.id','=','businesses.id_subcategory')
                    ->join('prices','prices.id','=','contact_business.price')
                    ->where('businesses.id','=', $id_business)
                    ->get();

        $images = Photo::select('id_business','perfil')
                        ->where('id_business','=',$id_business)
                        ->where('perfil','<>',"")
                        ->get();               

        return view('find_table')->with('business', $business)->with('images',$images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
