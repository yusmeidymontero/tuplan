@extends('layouts.admin')
@section('titulo','Resultados')
@section('content')
    <!-- Page Content -->
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ url('/') }}">TuPlan</a>
        </li>
        <li class="breadcrumb-item active">Búsqueda</li>
      </ol>
      <header class="business-subheader">
          <div class="col-lg-12 col-md-8 col-xs-6">
            <div id="custom-search-input" align="center">
              <br><br>
                  <div class="input-group col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <span class="input-group-addon">
                          <strong><i>¿Cuál es tu plan?</i></strong>
                      </span>
                      <input id="search" type="text" class="form-control input-lg" placeholder="Hamburguesas, Moros, Spa, Cervezas.." list="datalist1" />
                      <span class="input-group-btn">
                          <button class="btn button-danger" type="button" onclick="action_search()">
                          <i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
                      </span>
                      <br>
                  </div>
                  <br>
                   <div class="col-lg-6 collapse navbar-collapse pagination justify-content-center">
                    <form class="form-inline my-2 my-lg-0">
                      <button class="btn button-danger" onclick="action_search_category(1)">Restaurantes</button>
                      <button class="btn button-danger" onclick="action_search_category(3)">Salud y Belleza</button>
                      <button class="btn button-danger" onclick="action_search_category(4)">Bares</button>
                      <button class="btn button-danger">Servicios Profesionales</button>
                    </form>  
                  </div>  
                  <datalist id="datalist1">
                    @foreach($categories as $category)
                      <option data-id_category="{{$category->id}}" value="{{$category->name}}">
                      </option>  
                    @endforeach
                    @foreach($subcategories as $subcategory)
                      @if($subcategory->name != "N/A")
                        <option data-id_subcategory="{{$subcategory->id}}" value="{{$subcategory->name}}"></option>
                      @endif  
                    @endforeach
                  </datalist>
              </div>
          </div>
        </header>
          <nav class="navbar navbar-expand-lg navbar-light bg-faded">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link">Resultados para <strong>{{$search_text}}</strong></a>
                    <input type="text" name="search_by" id="search_by" hidden="" value="{{$search_text}}">
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="searchZone('NORTE')">Norte</button>
                <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="searchZone('CENTRO SUR')">Centro Sur</button>
                <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="searchZone('VIA A SAMBORONDÓN')">Via a Samborondón</button>    
            </form>
          </div>
          </nav>
          <div id="modal" align="center">
            <img src="../images/loader.gif" id="loader" width="50" hspace="16" height="50" vspace="16" />
          </div>
          <!--div class="form-group row">
          <h5>Resultados para <strong>{{$search_text}}</strong></h5>
          <div class="container" align="right">
            <div class="btn-group" role="group">
              <button type="button" class="btn btn-danger">Norte</button>
              <button type="button" class="btn btn-danger">Sur</button>
              <button type="button" class="btn btn-danger">Vía-Samborondón</button>
            </div>
          </div>
        </div-->
          <!--div class="form-check">
            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios1" value="option1" checked>
            <label class="form-check-label" for="exampleRadios1">
              <span class="fa fa-calendar"> Hacer Reservación</span>
            </label>
            <div class="btn-group">
              <button type="button" class="btn btn-danger">Fecha</button>
              <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <div class="btn-group">
              <button type="button" class="btn btn-danger">Hora</button>
              <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              </div>
              <button type="button" class="btn btn-danger">Personas</button>
              <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
              </button>
            </div>
          </div-->

        <!-- Blog Post -->
        <div id="search_result">
          <div id="search_remove">
            @foreach($search as $busi)
              <div class="card mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-4">
                      <?php $i = 0; ?>
                      @foreach($images as $image)
                      @if($busi->id == $image[$i]->id_business)
                        @if(is_null($image[$i]->perfil))
                        <img class="card-img-top" src="http://placehold.it/250x100" alt="">
                        @else
                        <img class="card-img-top" src="{{ asset($image[$i]->perfil) }}" width="300" height="200" alt="">
                        @endif
                      @endif
                        <?php $i = $i++; ?>
                      @endforeach
                    </div>
                    <div class="col-lg-8">
                      <h2 class="card-title"><a href="{{ url('business/detail/'.$busi->id) }}">
                        <font color="red">{{$busi->name}}</font></a></h2>
                      <p class="card-text">
                        {{$busi->category}} / {{$busi->subcategory}}
                        <br>
                        Dirección: {{$busi->address}}
                        <br>
                        Zona: {{$busi->place}}
                      </p>
                      <a href="{{ url('business/detail/'.$busi->id) }}" class="btn button-danger">Reservar &rarr;</a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            <div class="pagination justify-content-center">
              {{ $search->links() }}
            </div> 
          </div>
        </div>  
    </div>  
    <script type="text/javascript">
      $("#price").button('toggle');
    </script>
    <script type="text/javascript">
      function action_search(){
           var val = $('#search').val();
           var url = '/search/filter/'+val;
           window.location = url;
      }
    </script>

    <script type="text/javascript">
      function action_search_category(category){
        var url = '/search/'+category;
        window.location = url;
      }
    </script>
    <script type="text/javascript">
      function searchZone(zone){
        var input_text = $('#search_by').val();
        openModal();
        $(function(){
            $.ajax({
                type:'get',
                url: '/search/zone/'+input_text+'/'+zone,
                success: function(data){
                  document.getElementById("search_remove").remove();
                  $('#search_result').empty().html(data);
                  closeModal();
                }
            });
        });
      }
    </script>
    <script type="text/javascript">
      function openModal() {
        document.getElementById('modal').style.display = 'block';
      }

      function closeModal() {
          document.getElementById('modal').style.display = 'none';
      }
    </script>
@endsection
