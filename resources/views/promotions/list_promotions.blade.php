<div class="row" id="block-promotions">
        @foreach($promotions as $promo)
        <div class="col-sm-4 my-4">
          <div class="card">
              @if(is_null($promo->perfil))
              <img class="card-img-top" src="http://placehold.it/300x200" alt="">
              @else
              <img class="card-img-top" src="{{ asset($promo->perfil) }}" width="300" height="200" alt="">
              @endif
            <div class="card-body">
              <h4 class="card-title">{{$promo->name}}</h4>
            </div>
            <div class="card-footer" align="center">
                <a href="{{ url('reservations/'.$promo->id) }}" class="btn button-danger btn-lg btn-block">RESERVAR</a>
            </div>
          </div>
        </div>
        @endforeach
</div>
<!--div class="pagination justify-content-center">
        {{ $promotions->links() }}
</div-->